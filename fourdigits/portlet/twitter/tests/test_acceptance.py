import unittest

from plone.testing import layered

from fourdigits.portlet.twitter.testing import (
    FOURDIGITS_PORTLET_TWITTER_ACCEPTANCE_TESTING
)

import robotsuite


def test_suite():
    suite = unittest.TestSuite()
    suite.addTests([
        layered(robotsuite.RobotTestSuite(
            "acceptance/test_twitterportlet.txt"),
            layer=FOURDIGITS_PORTLET_TWITTER_ACCEPTANCE_TESTING),
    ])
    return suite
