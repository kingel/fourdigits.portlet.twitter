import unittest2 as unittest

from fourdigits.portlet.twitter.testing import \
    FOURDIGITS_PORTLET_TWITTER_INTEGRATION_TESTING

class TestSetup(unittest.TestCase):

    layer = FOURDIGITS_PORTLET_TWITTER_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']

    def test_browserlayer_available(self):
        from plone.browserlayer import utils
        from fourdigits.portlet.twitter.interfaces import \
            IFourdigitsPortletTwitter
        self.failUnless(IFourdigitsPortletTwitter in utils.registered_layers())


def test_suite():
    return unittest.defaultTestLoader.loadTestsFromName(__name__)
