# -*- coding: utf-8 -*-
import unittest2 as unittest
from plone.testing.z2 import Browser
from plone.app.testing import TEST_USER_ID
from plone.app.testing import SITE_OWNER_NAME
from plone.app.testing import SITE_OWNER_PASSWORD
from plone.app.testing import setRoles
from zope.component import getUtility, getMultiAdapter
from zope.site.hooks import setHooks

from plone.portlets.interfaces import IPortletType
from plone.portlets.interfaces import IPortletManager
from plone.portlets.interfaces import IPortletAssignment
from plone.portlets.interfaces import IPortletDataProvider
from plone.portlets.interfaces import IPortletRenderer

from plone.app.portlets.storage import PortletAssignmentMapping

from fourdigits.portlet.twitter import fourdigitsportlettwitter as twitter
from fourdigits.portlet.twitter.testing import \
    FOURDIGITS_PORTLET_TWITTER_INTEGRATION_TESTING


class TestPortlet(unittest.TestCase):

    layer = FOURDIGITS_PORTLET_TWITTER_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        setHooks()

    def testPermission(self):
        permissions = [p['name'] for p in self.portal.permission_settings()]
        self.failUnless(
            'fourdigits.portlet.twitter: Add FourdigitsPortletTwitter' in
            permissions)

    def testPortletTypeRegistered(self):
        portlet = getUtility(IPortletType,
            name='fourdigits.portlet.twitter.FourdigitsPortletTwitter')
        self.assertEquals(portlet.addview,
            'fourdigits.portlet.twitter.FourdigitsPortletTwitter')

    def testInterfaces(self):
        portlet = twitter.Assignment(name="foo")
        self.failUnless(IPortletAssignment.providedBy(portlet))
        self.failUnless(IPortletDataProvider.providedBy(portlet.data))

    def testInvokeAddview(self):
        portlet = getUtility(IPortletType,
            name='fourdigits.portlet.twitter.FourdigitsPortletTwitter')
        mapping = self.portal.restrictedTraverse(
            '++contextportlets++plone.leftcolumn')
        for m in mapping.keys():
            del mapping[m]
        addview = mapping.restrictedTraverse('+/' + portlet.addview)

        addview.createAndAdd(data={'name': u"test title",
                                   'username': u"plone"})

        self.assertEquals(len(mapping), 1)
        self.failUnless(isinstance(mapping.values()[0], twitter.Assignment))

    def testInvokeEditView(self):
        mapping = PortletAssignmentMapping()
        request = self.portal.REQUEST

        mapping['foo'] = twitter.Assignment(name=u"title", username="plone")
        editview = getMultiAdapter((mapping['foo'], request), name='edit')
        self.failUnless(isinstance(editview, twitter.EditForm))

    def testRenderer(self):
        context = self.portal
        request = self.portal.REQUEST
        view = self.portal.restrictedTraverse('@@plone')
        manager = getUtility(IPortletManager,
            name='plone.leftcolumn', context=self.portal)
        assignment = twitter.Assignment(name=u"title", username="plone")

        renderer = getMultiAdapter(
            (context, request, view, manager, assignment),
            IPortletRenderer)
        self.failUnless(isinstance(renderer, twitter.Renderer))
        self.failUnless(renderer.available,
                        "Renderer should be available by default.")


class TestRenderer(unittest.TestCase):

    layer = FOURDIGITS_PORTLET_TWITTER_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        setHooks()

    def renderer(self, context=None, request=None, view=None, manager=None,
                 assignment=None):
        context = context or self.portal
        request = request or self.portal.REQUEST
        view = view or self.portal.restrictedTraverse('@@plone')
        manager = manager or getUtility(IPortletManager,
            name='plone.leftcolumn', context=self.portal)
        assignment = assignment or twitter.Assignment(name=u"title",
                                                      username="plone")

        return getMultiAdapter((context, request, view, manager, assignment),
                               IPortletRenderer)

    def test_show_title(self):
        r = self.renderer(
            context=self.portal,
            assignment=twitter.Assignment(name=u"title",
                                          username="plone", )
        )
        r = r.__of__(self.portal)
        r.update()
        output = r.render()
        self.failUnless('title' in output)

    def test_show_twitterlink(self):
        r = self.renderer(
            context=self.portal,
            assignment=twitter.Assignment(username="plone")
        )
        r = r.__of__(self.portal)
        r.update()
        output = r.render()
        self.failUnless('<a href="http://twitter.com/plone">plone</a>' in
                        output)

    def test_show_userinfo(self):
        r = self.renderer(
            context=self.portal,
            assignment=twitter.Assignment(username="plone", userinfo=True)
        )
        r = r.__of__(self.portal)
        r.update()
        output = r.render()
        self.failUnless("On the internets" in output)

    def test_show_userpictures(self):
        r = self.renderer(
            context=self.portal,
            assignment=twitter.Assignment(username="plone",
                                          userinfo=True,
                                          userpictures=True)
        )
        r = r.__of__(self.portal)
        r.update()
        output = r.render()
        self.failUnless("img alt=\"Twitter user picture\" src=" in output)

    def test_use_searchterms(self):
        r = self.renderer(
            context=self.portal,
            assignment=twitter.Assignment(username="plone",
                                          userinfo=True,
                                          search="plone")
        )
        r = r.__of__(self.portal)
        r.update()
        output = r.render()
        self.failUnless("plone" in output)

    def test_use_filtertext(self):
        r = self.renderer(
            context=self.portal,
            assignment=twitter.Assignment(username="plone",
                                          userinfo=True,
                                          search="plone",
                                          filtertext="joomla")
        )
        r = r.__of__(self.portal)
        r.update()
        output = r.render()
        self.assertNotIn("joomla", output)

    def test_use_mergeuserandsearch(self):
        r = self.renderer(
            context=self.portal,
            assignment=twitter.Assignment(username="plone",
                                          userinfo=True,
                                          search="plone",
                                          mergeuserandsearch=True)
        )
        r = r.__of__(self.portal)
        r.update()
        output = r.render()
        self.failUnless("plone" in output)

    def testHide(self):
        self.assertRaises(TypeError, twitter.Assignment, hide=True)


def test_suite():
    return unittest.defaultTestLoader.loadTestsFromName(__name__)
