import doctest

from zope.configuration import xmlconfig

from plone.testing import z2

from plone.app.testing import TEST_USER_ID
from plone.app.testing import setRoles
from plone.app.testing import login
from plone.app.testing import PloneSandboxLayer
from plone.app.testing import applyProfile
from plone.app.testing.layers import FunctionalTesting
from plone.app.testing.layers import IntegrationTesting


class FourdigitsPortletTwitterLayer(PloneSandboxLayer):

    def setUpZope(self, app, configurationContext):
        import fourdigits.portlet.twitter
        xmlconfig.file(
            'configure.zcml',
            fourdigits.portlet.twitter,
            context=configurationContext)

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'fourdigits.portlet.twitter:default')


FOURDIGITS_PORTLET_TWITTER_FIXTURE = FourdigitsPortletTwitterLayer()

FOURDIGITS_PORTLET_TWITTER_INTEGRATION_TESTING = IntegrationTesting(
    bases=(FOURDIGITS_PORTLET_TWITTER_FIXTURE,),
    name="FourdigitsPortletTwitter:Integration")
FOURDIGITS_PORTLET_TWITTER_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(FOURDIGITS_PORTLET_TWITTER_FIXTURE,),
    name="FourdigitsPortletTwitter:Functional")
FOURDIGITS_PORTLET_TWITTER_ACCEPTANCE_TESTING = FunctionalTesting(
    bases=(FOURDIGITS_PORTLET_TWITTER_FIXTURE, z2.ZSERVER_FIXTURE),
    name="FourdigitsPortletTwitter:Acceptance")

optionflags = (doctest.ELLIPSIS | doctest.NORMALIZE_WHITESPACE)
